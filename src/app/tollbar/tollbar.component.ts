import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";



@Component({
  selector: 'app-tollbar',
  templateUrl: './tollbar.component.html',
  styleUrls: ['./tollbar.component.scss']
})
export class TollbarComponent implements OnInit {

  items: MenuItem[];

ngOnInit() {
  this.items = [
    {
      label: 'Update',
      icon: 'pi pi-refresh'
    },
    {
      label: 'Delete',
      icon: 'pi pi-times'
    },
    {
      label: 'Angular Website',
      icon: 'pi pi-external-link',
      url: 'http://angular.io'
    },
    {
      label: 'Router',
      icon: 'pi pi-upload',
      routerLink: '/fileupload'
    }
  ];
}
}


